var gulp = require("gulp");
var sass = require("gulp-sass");

gulp.task("sass", function() {
  return gulp.src("./scss/schrecknet.scss")
  .pipe(sass())
  .pipe(gulp.dest("dist/css/schrecknet.css"));
});